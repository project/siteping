
Siteping for Drupal 6.x

Installation
------------
Siteping can be installed in the usual way; place it in your site's modules directory and enable it at admin/build/modules.

How It Works
------------
This module is incredibly simple.  All it does it register a menu callback using Drupal's default menu system.  By the nature of using the hook system, it means that by the time this module code is invoked, Drupal would have naturally bootstrapped fully (meaning that it would have tried to connect to the database, etc).  What this means for you is that you can test to see if your site is alive without doing something more intensive, like requesting the entire home page, and without doing something that wouldn't test the database, like request a text file.  This serves as a happy medium.

How To Use It
-------------
This is only really of service to you when you have some sort of external URL checker you can run (perhaps monit, a custom bash script, or some other service) that will request this URL, check the output of it, and notify you if it gets an unintended response.  That part is up to you.  Again, all this module does is provide an "OK" text response at YOURSITE.COM/siteping.

